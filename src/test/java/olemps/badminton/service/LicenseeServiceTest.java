package olemps.badminton.service;

import olemps.badminton.dao.LicenseeDAO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static reactor.test.StepVerifier.create;

class LicenseeServiceTest extends ServiceTest {

    @Autowired
    LicenseeService licenseeService;

    @Autowired
    LicenseeDAO licenseeDAO;

    @BeforeEach
    void setUp() {
        licenseeDAO.init();
    }

    @AfterEach
    void tearDown() {
        licenseeDAO.clean();
    }

    @Test
    void findAll_shouldSucceed() {
        create(licenseeService.findAll())
                .expectSubscription()
                .expectNextCount(licenseeDAO.count())
                .verifyComplete();
    }
}