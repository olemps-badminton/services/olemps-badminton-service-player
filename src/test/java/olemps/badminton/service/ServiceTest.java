package olemps.badminton.service;

import olemps.badminton.context.MongoDBContext;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles("test")
@SpringBootTest
@Testcontainers
public class ServiceTest {

    @RegisterExtension
    private static final MongoDBContext mongodb = new MongoDBContext();

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        mongodb.applyContextOn(registry);
    }
}
