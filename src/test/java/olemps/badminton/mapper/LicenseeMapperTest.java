package olemps.badminton.mapper;

import olemps.badminton.model.business.Licensee;
import olemps.badminton.model.dto.api.LicenseeResumeApiDTO;
import olemps.badminton.model.types.Gender;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static java.time.Month.JANUARY;
import static olemps.badminton.mapper.LicenseeMapper.toResumeApiDTO;
import static olemps.badminton.model.types.Gender.MALE;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class LicenseeMapperTest {

    @Test
    void toResumeApiDTO_shouldSucceed() {
        Licensee input = new Licensee(
                "00000000",
                "John",
                "DOE",
                MALE,
                LocalDate.of(2000, JANUARY, 1),
                "john.doe@mail.fr"
        );
        LicenseeResumeApiDTO output = toResumeApiDTO(input);
        assertThat(output).isNotNull();
        assertThat(output.id()).isEqualTo(input.id());
        assertThat(output.givenName()).isEqualTo(input.givenName());
        assertThat(output.familyName()).isEqualTo(input.familyName());
        assertThat(output.gender()).isEqualTo(input.gender());
    }
}