package olemps.badminton.dao;

import olemps.badminton.model.business.Licensee;
import olemps.badminton.model.types.Gender;
import olemps.badminton.repository.LicenseeRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static java.time.Month.JANUARY;
import static java.util.List.of;
import static olemps.badminton.model.types.Gender.MALE;

@Component
public record LicenseeDAO(
        LicenseeRepository licenseeRepository
) {

    private static final List<Licensee> DEFAULT_ENTITIES = of(
            new Licensee(
                    "00000000",
                    "John",
                    "DOE",
                    MALE,
                    LocalDate.of(2020, JANUARY, 1),
                    "john.doe@mail.fr"
            )
    );

    public void init() {
        licenseeRepository.saveAll(DEFAULT_ENTITIES).blockLast();
    }

    public void clean() {
        licenseeRepository.deleteAll().block();
    }

    public long count() {
        return licenseeRepository.count().block();
    }
}
