package olemps.badminton.context;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.testcontainers.containers.MongoDBContainer;

public class MongoDBContext implements Extension, BeforeAllCallback, AfterAllCallback {

    private static final MongoDBContainer MONGO_DB = new MongoDBContainer("mongo");

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        MONGO_DB.start();
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        MONGO_DB.stop();
    }

    public void applyContextOn(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", MONGO_DB::getReplicaSetUrl);
    }
}
