package olemps.badminton.documentation;

import org.springframework.restdocs.headers.HeaderDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.web.reactive.server.EntityExchangeResult;

import java.util.List;
import java.util.function.Consumer;

import static com.epages.restdocs.WireMockDocumentation.wiremockJson;
import static com.epages.restdocs.apispec.WebTestClientRestDocumentationWrapper.document;
import static java.util.List.of;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.payload.JsonFieldType.STRING;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;

public final class LicenseeDocumentation {

    private LicenseeDocumentation() {
    }

    public static <T> Consumer<EntityExchangeResult<T>> findAllDocumentation(String id) {
        return document(
                id,
                responseHeaders(responseHeaderDescription()),
                responseFields().andWithPrefix("[].", responseBodyDescription()),
                wiremockJson()
        );
    }

    private static List<HeaderDescriptor> responseHeaderDescription() {
        return of(
                headerWithName(CONTENT_TYPE).description(APPLICATION_JSON)
        );
    }

    private static List<FieldDescriptor> responseBodyDescription() {
        return of(
                fieldWithPath("id").type(STRING).description("License"),
                fieldWithPath("givenName").type(STRING).description("Given Name"),
                fieldWithPath("familyName").type(STRING).description("Family Name"),
                fieldWithPath("gender").type(STRING).description("Gender")
        );
    }
}
