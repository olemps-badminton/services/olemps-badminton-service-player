package olemps.badminton.resource;

import olemps.badminton.context.MongoDBContext;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureWebTestClient
@AutoConfigureRestDocs
@Testcontainers
public class ResourceTest {

    @RegisterExtension
    private static final MongoDBContext mongodb = new MongoDBContext();

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        mongodb.applyContextOn(registry);
    }

    @Autowired
    protected WebTestClient webTestClient;
}
