package olemps.badminton.resource;

import olemps.badminton.dao.LicenseeDAO;
import olemps.badminton.model.dto.api.LicenseeResumeApiDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.lang.Math.toIntExact;
import static olemps.badminton.documentation.LicenseeDocumentation.findAllDocumentation;
import static olemps.badminton.resource.URIConstants.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

class LicenseeResourceTest extends ResourceTest {

    @Autowired
    LicenseeDAO licenseeDAO;

    @BeforeEach
    void setUp() {
        licenseeDAO.init();
    }

    @AfterEach
    void tearDown() {
        licenseeDAO.clean();
    }

    @Test
    void findAll() {
        webTestClient
                .get()
                .uri(BASE_PATH + VERSION_1 + LICENSEES)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON)
                .expectBodyList(LicenseeResumeApiDTO.class)
                .consumeWith(r -> {
                    List<LicenseeResumeApiDTO> licensees = r.getResponseBody();
                    assertThat(licensees).hasSize(toIntExact(licenseeDAO.count()));
                })
                .consumeWith(findAllDocumentation("findAll"));
    }
}