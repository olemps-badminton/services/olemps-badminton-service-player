package olemps.badminton.resource;

public final class URIConstants {

    public static final String BASE_PATH = "/service-player";
    public static final String VERSION_1 = "/v1";

    public static final String LICENSEES = "/licensees";
    public static final String LICENSEE_ID = "licenseeId";
    public static final String LICENSEE_PATH = LICENSEES + "/{" + LICENSEE_ID + "}";

    private URIConstants() {}
}
