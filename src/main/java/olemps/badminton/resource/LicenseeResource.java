package olemps.badminton.resource;

import olemps.badminton.mapper.LicenseeMapper;
import olemps.badminton.model.dto.api.LicenseeResumeApiDTO;
import olemps.badminton.service.LicenseeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import static olemps.badminton.resource.URIConstants.*;

@RestController
@RequestMapping(BASE_PATH)
public record LicenseeResource(LicenseeService licenseeService) {

    @GetMapping
    @RequestMapping(VERSION_1 + LICENSEES)
    public Flux<LicenseeResumeApiDTO> findAll() {
        return licenseeService
                .findAll()
                .map(LicenseeMapper::toResumeApiDTO);
    }
}
