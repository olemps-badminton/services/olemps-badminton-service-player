package olemps.badminton.service;

import olemps.badminton.model.business.Licensee;
import olemps.badminton.repository.LicenseeRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public record LicenseeService(LicenseeRepository licenseeRepository) {

    public Flux<Licensee> findAll() {
        return licenseeRepository.findAll();
    }
}
