package olemps.badminton.repository;

import olemps.badminton.model.business.Licensee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LicenseeRepository extends ReactiveMongoRepository<Licensee, String> {
}
