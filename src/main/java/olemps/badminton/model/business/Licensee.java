package olemps.badminton.model.business;

import olemps.badminton.model.types.Gender;

import java.time.LocalDate;

public record Licensee(
        String id,
        String givenName,
        String familyName,
        Gender gender,
        LocalDate medicalCertificateUpdatedAt,
        String mailAddress
) {
}
