package olemps.badminton.model.dto.api;

import olemps.badminton.model.types.Gender;

public record LicenseeResumeApiDTO(
        String id,
        String givenName,
        String familyName,
        Gender gender
) {
}
