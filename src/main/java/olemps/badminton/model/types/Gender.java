package olemps.badminton.model.types;

public enum Gender {
    MALE,
    FEMALE
}
