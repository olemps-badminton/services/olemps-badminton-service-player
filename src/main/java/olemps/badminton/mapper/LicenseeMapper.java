package olemps.badminton.mapper;

import olemps.badminton.model.business.Licensee;
import olemps.badminton.model.dto.api.LicenseeResumeApiDTO;

public final class LicenseeMapper {

    private LicenseeMapper() {}

    public static LicenseeResumeApiDTO toResumeApiDTO(Licensee input) {
        return new LicenseeResumeApiDTO(
                input.id(),
                input.givenName(),
                input.familyName(),
                input.gender()
        );
    }
}
